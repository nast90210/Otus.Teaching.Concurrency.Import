﻿using System;
using System.Collections.Generic;
using System.Threading;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Entities;


namespace Otus.Teaching.Concurrency.Import.Cli
{
    class Program
    {
        private static readonly HttpClient client = new HttpClient();
        
        static async Task Main(string[] args)
        {
            Console.WriteLine("Для выхода из программы нажмите Q");
            Console.WriteLine("");
            
            do
            {
                Console.Clear();
                Console.WriteLine("Введите R для создания случайного Customer");
                Console.WriteLine("Введите A для получения всех Customer");
                
                var key = Console.ReadKey();
                Console.WriteLine();
                
                switch (key.Key)
                {
                    case ConsoleKey.Q:
                        Console.WriteLine("Выход из приложения");
                        Thread.Sleep(500);
                        return;
                    case ConsoleKey.A:
                        var customersCollection = await client.GetFromJsonAsync<List<Customer>>("https://localhost:5001/Users");
                        
                        if (customersCollection != null)
                        {
                            foreach (var customer in customersCollection)
                            {
                                Console.WriteLine(customer.ToString());
                                Console.WriteLine();
                                Thread.Sleep(500);
                            }
                        }   

                        break;
                    case ConsoleKey.R:
                        var customers = RandomCustomerGenerator.Generate(10);
                        foreach (var customer in customers)
                        {
                            var response = await client.PostAsJsonAsync("https://localhost:5001/Users", customer);
                            var responseString = await response.Content.ReadAsStringAsync();
                            Console.WriteLine(responseString);
                            Thread.Sleep(500);
                        }
                        break;
                    default:
                        Console.WriteLine($"Неизвестная клавиша {key.KeyChar}");
                        Thread.Sleep(500);
                        break;
                }
            } while (true);

        }
    }
}