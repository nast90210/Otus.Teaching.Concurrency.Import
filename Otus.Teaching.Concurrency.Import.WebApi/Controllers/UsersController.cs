using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Loader.Context;

namespace Otus.Teaching.Concurrency.Import.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private CustomerContext _db;
        
        public UsersController(CustomerContext db)
        {
            _db = db;
        }
        
        /// <summary>
        /// Получить коллекцию всех Customers
        /// </summary>
        /// <returns>Customers collection</returns>
        [HttpGet]
        public async Task<List<Customer>> Get()
        {
            return await _db.Customers.ToListAsync();
        }

        [HttpGet("id")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetById(int id)
        {
            var customer = await _db.Customers.FindAsync(id);
            if (customer == null)
                return NotFound();
            return Ok(customer);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult> Post([FromBody] Customer customer)
        {

            if (await _db.Customers.FindAsync(customer.Id) != null)
            {
                return Conflict($"Сustomer with id {customer.Id} already exist");
            }
            _db.Customers.Add(customer);
            await _db.SaveChangesAsync();
            
            return CreatedAtAction(nameof(GetById), new { id = customer.Id }, customer);
        }
    }
}