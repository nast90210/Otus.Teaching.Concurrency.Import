using System.Collections.Generic;
using System.IO;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser : IDataParser<List<Customer>>
    {
        private readonly string _file;

        public CsvParser(string file)
        {
            _file = file;
        }
        public List<Customer> Parse()
        {
            var customers = new List<Customer>();
            using var reader = File.OpenText(_file);
            do
            {
                var line = reader.ReadLine();
                if (line == null)
                    break;
                customers.Add(new Customer(line));
            } while (true);
            return customers;
        }
    }
}