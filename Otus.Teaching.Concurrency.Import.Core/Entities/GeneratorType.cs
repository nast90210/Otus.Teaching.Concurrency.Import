using System;

namespace Otus.Teaching.Concurrency.Import.Handler.Entities
{
    public enum GeneratorType
    {
        csv,
        xml
    }
}