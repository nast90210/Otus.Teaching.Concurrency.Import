namespace Otus.Teaching.Concurrency.Import.Handler.Entities
{
    public class Customer
    {
        public Customer(string line)
        {
            var array = line.Split(",");
            Id = int.Parse(array[0]);
            FullName = array[1];
            Email = array[2];
            Phone = array[3];
        }

        public Customer()
        {
            
        }

        public int Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public override string ToString()
        {
            return Id + "," + FullName + "," + Email + "," + Phone;
        }
    }
}