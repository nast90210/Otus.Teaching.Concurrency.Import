using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Loader.Context;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class ThreadDataLoader : IDataLoader
    {
        private readonly CustomerContext _db;
        private readonly List<Customer> _customers;
        private static readonly object Locker = new object();
        
        public ThreadDataLoader(CustomerContext db, List<Customer> customers)
        {
            _db = db;
            _customers = customers;
        }
        
        public void LoadData()
        {
            lock (Locker)
            {
                _db.Customers.AddRange(_customers);
                _db.SaveChanges();    
            }
        }
    }
}