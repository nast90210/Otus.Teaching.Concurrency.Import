﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Loader.Context;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    internal class Program
    {
        private const string App = "/Users/mikhail/Projects/otus/otus_hw8_parallels/" +
                                   "Otus.Teaching.Concurrency.Import.DataGenerator.App/" +
                                   "bin/Release/netcoreapp3.1/publish/Otus.Teaching.Concurrency.Import.DataGenerator.App";

        private static readonly string DataFileName =
            Path.Combine(AppDomain.CurrentDomain.BaseDirectory!, "customers.csv");

        private const int DataCount = 100;
        private static GeneratorStartType _generatorStartType;

        private static void Main(string[] args)
        {
            if (args is { Length: 1 })
                _generatorStartType = Enum.Parse<GeneratorStartType>(args[0]);

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            GenerateCustomersDataFile();

            var customers = new CsvParser(DataFileName).Parse();
            
            var db = new CustomerContext();
            
            try
            {
                var count = 0;
                do
                {
                    if(count + 10 > customers.Count)
                        break;
                    var thread = new Thread(() => new ThreadDataLoader(db, customers.GetRange(count, 10)).LoadData());
                    thread.Start();
                    thread.Join();
                    count += 10;
                    
                } while (true);
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

            Console.WriteLine("Finish");
            
        }

        private static void GenerateCustomersDataFile()
        {
            switch (_generatorStartType)
            {
                case GeneratorStartType.proc:
                    var startInfo = new ProcessStartInfo
                    {
                        ArgumentList = { DataFileName, DataCount.ToString() },
                        FileName = App
                    };
                    var process = Process.Start(startInfo);
                    process?.WaitForExit();
                    break;
                case GeneratorStartType.meth:
                    var generator = new CsvGenerator(DataFileName, DataCount);
                    generator.Generate();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(_generatorStartType.ToString());
            }
        }

        private enum GeneratorStartType
        {
            proc,
            meth
        }
    }
}