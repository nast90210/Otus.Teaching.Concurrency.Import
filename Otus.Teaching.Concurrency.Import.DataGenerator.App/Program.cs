﻿using System;
using System.IO;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    class Program
    {
        private static string _dataFileName; 
        private static int _dataCount = 100; 

        private static GeneratorType _generatorType;

        private static void Main(string[] args)
        {
            if (!TryValidateAndParseArgs(args))
                return;
            
            Console.WriteLine($"Generating {_generatorType} data...");

            var generator = GeneratorFactory.GetGenerator(_dataFileName, _dataCount, _generatorType);
            
            generator.Generate();
            
            Console.WriteLine($"Generated {_generatorType} data in {_dataFileName}...");
        }

        private static bool TryValidateAndParseArgs(string[] args)
        {
            if(args != null && args.Length > 0)
            {
                switch (args.Length)
                {
                    case 3:
                        if (!Enum.TryParse(args[2], out _generatorType))
                            return false;
                        goto case 2;
                    case 2:
                        if(!int.TryParse(args[1], out _dataCount))
                            return false;
                        goto case 1;
                    case 1:
                        _dataFileName = $"{args[0]}"; //Path.Combine(_dataFileDirectory, $"{args[0]}.{_generatorType}");
                        break;

                }
            }
            else
            {
                Console.WriteLine("Data file name without extension is required");
                return false;
            }
            return true;
        }
    }
}