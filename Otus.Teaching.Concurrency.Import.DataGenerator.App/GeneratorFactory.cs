using System;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    public static class GeneratorFactory
    {
        public static IDataGenerator GetGenerator(string fileName, int dataCount, GeneratorType generatorType)
        {
            return generatorType switch
            {
                GeneratorType.xml => new XmlDataGenerator(fileName, dataCount),
                GeneratorType.csv => new CsvGenerator(fileName, dataCount),
                _ => throw new ArgumentOutOfRangeException(nameof(generatorType), generatorType, null)
            };
        }
    }
}